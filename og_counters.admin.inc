<?php
/**
 * @file
 * Admin pages for the Organic groups counters module.
 */

/**
 * Form constructor of the OG counters settings form.
 */
function og_counters_settings_form() {
  $form = array();

  // No way I will let this array go beyond the 80th column, nor be
  // split in several lines, so...
  // @codingStandardsIgnoreStart
  $intervals = array(30, 60, 120, 300, 900, 3600, 7200,
                     14400, 28800, 43200, 86400);
  // @codingStandardsIgnoreEnd
  $options = array(
    -1 => t('None'),
    0 => t('On each request'),
  );

  foreach ($intervals as $i) {
    $options[$i] = t('Every !interval',
                     array('!interval' => format_interval($i)));
  }

  $form['og_counters_update_frequency'] = array(
    '#type' => 'select',
    '#title' => t('Update frequency'),
    '#required' => TRUE,
    '#options' => $options,
    '#default_value' => variable_get('og_counters_update_frequency', 120),
    '#description' => t('Set to <em>None</em> to disable automatic updates, or <em>On each request</em> to trigger an update at every page request.'),
  );

  $form['#submit'] = array('og_counters_settings_form_submit');

  return system_settings_form($form);
}

/**
 * Submit handler for the OG counters settings form.
 */
function og_counters_settings_form_submit($form, &$form_state) {
  db_delete('flood')
    ->condition('event', 'og_counters_update_user')
    ->execute();
}
