<?php
/**
 * @file
 * Schema and uninstall hooks for the Organic groups counters module.
 */

/**
 * Implements hook_schema().
 */
function og_counters_schema() {
  $schema = array();

  $schema['og_counters'] = array(
    'description' => 'Table to maintain per-user/group counters.',
    'fields' => array(
      'ogcid' => array(
        'description' => 'The counters ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => "The group's node ID.",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the counters were last updated.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'new_posts' => array(
        'description' => 'The number of new posts since last login.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'new_comments' => array(
        'description' => 'The number of new comments since last login.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'uid_nid' => array('uid', 'nid'),
    ),
    'primary key' => array('ogcid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function og_counters_uninstall() {
  variable_del('og_counters_update_frequency');
}
